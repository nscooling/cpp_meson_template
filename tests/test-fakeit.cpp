#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "fakeit.hpp"

using namespace fakeit;

#include <iostream>
#include <string>

struct SomeInterface
{
  virtual int foo(int)         = 0;
  virtual int bar(std::string) = 0;
  virtual ~SomeInterface()     = default;
};

TEST_CASE("testing FakeIt")
{
  // Instantiate a mock object.
  Mock<SomeInterface> mock;

  // Setup mock behavior.
  When(Method(mock, foo)).Return(1); // Method mock.foo will return 1 once.

  // Fetch the mock instance.
  SomeInterface& i = mock.get();

  CHECK(i.foo(0) == 1);
}